/**
 * 2017年5月28日
 */
package cn.edu.bjtu.datasource.lsp;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.datavec.api.records.reader.impl.regex.RegexLineRecordReader;
import org.datavec.api.writable.Writable;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.iterator.LabeledSentenceProvider;
/**
 * 在driver包中不要直接使用,而是使用FDLabeledSentenceExceptionSupportProvider
 * @author Alex
 *
 */
class FDSentenceProvider extends RegexLineRecordReader implements LabeledSentenceProvider {
	private static final long serialVersionUID = 6692035412413143379L;
	List<String> labels = new ArrayList<String>(30);
	public FDSentenceProvider(String regex) {
		super(regex, 0);
		File fs[] = new File("D:\\Share\\answer").listFiles();
		for(File f:fs){
			labels.add(f.getName());
		}
	}
	//FIXME:修正
	public FDSentenceProvider() {
		this("^\\d{1,2}\\s(.*?)\\d{1,4}\\.txt\\s+(.*)");
	}
	//FIXME:修正
	@Override
	public Pair<String, String> nextSentence() {
		List<Writable> lists = next();
		String label = lists.get(0).toString();
		String content = lists.get(1).toString();
		return Pair.<String,String>makePair(content, label);
	}
	@Override
	public int totalNumSentences() {
		return 10000;
	}

	@Override
	public List<String> allLabels() {
		return Collections.unmodifiableList(labels);
	}

	@Override
	public int numLabelClasses() {
		return labels.size();
	}
	public int[] getSplitAndLineIndex(){
		return new int[]{this.splitIndex,this.lineIndex};
	}
}
