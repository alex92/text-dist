/**
 * 2017年5月23日
 */
package cn.edu.bjtu.datasource.lsp;

import java.util.List;

import org.datavec.api.writable.Writable;
import org.deeplearning4j.berkeley.Pair;

/**
 * @author Alex
 *
 */
public class MultiLabelTrainCNNSentenceProvider extends TrainCNNSentenceProvider {

	private static final long serialVersionUID = 951939813646416041L;
	public Pair<String, String> nextSentence() {
		List<Writable> l = next();
		labels = l.get(0).toString();
		content = l.get(1).toString().replaceAll("[\\\\r\\\\n\\s]", "");
		
		while(!shouldKeep(content) && hasNext()){
			List<Writable> temp = next();
			labels = temp.get(0).toString();
			content = temp.get(1).toString();
		}
		if(!shouldKeep(content)){
			logger.info("At split {} at lineIndex {} ,content is {} not statisfied",this.splitIndex,this.lineIndex,content);
			throw new RuntimeException("Illegal DataSet");
		}
		return Pair.makePair(content, labels);
	}
}
