/**
 * 2017年5月28日
 */
package cn.edu.bjtu.datasource.lsp;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.datavec.api.split.FileSplit;
import org.deeplearning4j.berkeley.Pair;
import org.deeplearning4j.iterator.LabeledSentenceProvider;
import cn.edu.bjtu.core.LoggerSupport;

/**
 * @author Alex
 *
 */
public class FDLabeledSentenceExceptionSupportProvider extends LoggerSupport implements LabeledSentenceProvider {
	FDSentenceProvider fds ;
	Pair<String,String> next = null;
	public FDLabeledSentenceExceptionSupportProvider(String path) throws IOException, InterruptedException {
		fds = new FDSentenceProvider();
		fds.initialize(new FileSplit(new File(path)));
		
	}
	@Override
	public boolean hasNext() {
		next = null;
		while(next == null && fds.hasNext()){
			try{
				next = fds.nextSentence();
			}catch(Exception e){
				logException(e);
				next = null;
			}
		}
		return next != null;
	}
	@Override
	public Pair<String, String> nextSentence() {
		return next;
	}
	@Override
	public void reset() {
		fds.reset();
	}
	@Override
	public int totalNumSentences() {
		return 10000;
	}
	@Override
	public List<String> allLabels() {
		return fds.allLabels();
	}
	@Override
	public int numLabelClasses() {
		return fds.allLabels().size();
	}
	public int [] getSplitAndLineIndex(){
		return fds.getSplitAndLineIndex();
	}
}
