/**
 * 2017年6月25日
 */
package cn.edu.bjtu.api;

/**
 * @author Alex
 *
 */
public interface LabelManagerService {
	String[] getLabels();
 }
