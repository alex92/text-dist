/**
 * 2017年6月25日
 */
package cn.edu.bjtu.api;

import java.util.Date;

import cn.edu.bjtu.core.Deep4jModelType;


/**
 * @author Alex
 *
 */
public interface NetworkModelService {
	Date lastModifiedTime();
	Deep4jModelType getType();
	int getSentenceLength();
	byte[] getModelFileStream();
}
