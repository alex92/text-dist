package cn.edu.bjtu.api;

import cn.edu.bjtu.core.ClassificationPair;

/**
 * 暂时不支持训练网络
 * Created by alex on 17/6/16.
 */
public interface ClassificationService {
    ClassificationPair[] classifyDocument(String doc) throws Exception;
    ClassificationPair[][] classifyDocument(String[] docs) throws Exception;
}
