/**
 * NeuronNetwork.java created by zhangzhidong 
 * at 上午10:04:32 2017年5月26日
 */
package cn.edu.bjtu.api;

import java.util.concurrent.Future;

import org.deeplearning4j.eval.Evaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import cn.edu.bjtu.core.Deep4jModelType;

/**
 * 这个接口里面的方法的层次比较低，一般实现这个接口的类都是将这些方法直接委托到 DEEP4J 里面的模型。
 * @author zhangzhidong<br>
 * comment generated at 2017年5月26日上午10:04:32<br>
 * 
 */
public interface NeuronNetwork {
	public INDArray[] output(INDArray... input) throws Exception;
	public Future<INDArray[]> outputAsync(INDArray... input) throws Exception;
	
	public Evaluation evaluate(DataSetIterator iterator) throws Exception ;
	public Future<Evaluation> evaluateAsync(DataSetIterator iterator) throws Exception ;
	
	
	Deep4jModelType getActualType();
	public void load() throws Exception;
}
