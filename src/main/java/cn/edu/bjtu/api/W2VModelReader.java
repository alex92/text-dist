/**
 * 
 */
package cn.edu.bjtu.api;

import java.io.File;

/**
 * @author alex
 *
 */
public interface W2VModelReader {
	public File getW2vFile() throws Exception;
	public void release();
}
