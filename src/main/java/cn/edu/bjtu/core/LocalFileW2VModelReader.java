 /**
 * 
 */
package cn.edu.bjtu.core;

import java.io.File;
import java.io.InputStream;
import java.io.Serializable;

import org.apache.commons.io.FileUtils;

import cn.edu.bjtu.api.W2VModelReader;

/**
 * @author alex
 *
 */
public class LocalFileW2VModelReader extends LoggerSupport implements W2VModelReader,Serializable{
	File temp = null;
	@Override
	public File getW2vFile() throws Exception{
		temp = File.createTempFile("word2vec", "wordembedding");
		temp.deleteOnExit();
		InputStream in = this.getClass().getClassLoader().getResourceAsStream("model/fdModel_100.txt");
		logger.info("Loading w2v model .... {} ",temp.getAbsolutePath());
		FileUtils.copyInputStreamToFile(in, temp);
		return temp;
	}
	/* (non-Javadoc)
	 * @see cn.edu.bjtu.api.W2VModeReader#release()
	 */
	@Override
	public void release() {
		if(temp!=null){
			if(temp.exists()){
				temp.delete();
				logger.warn(" delete temp file  ... {} ",temp.getAbsolutePath());
			}else{
				logger.warn(" {}  not exist " , temp.getAbsolutePath() );
			}
		}
	}
	

}
