/**
 * Deep4jModelType.java created by zhangzhidong 
 * at 上午10:21:39 2017年5月26日
 */
package cn.edu.bjtu.core;

/**
 * 以后在有100种网络这个类也能表达了
 * 这两个类没有继承关系，所以用个枚举表达了
 * comment generated at 2017年5月26日上午10:21:39<br>
 * 
 */
public enum Deep4jModelType {
	ComputationGraph,MultiLayerNetwork,Fake,Local
}
