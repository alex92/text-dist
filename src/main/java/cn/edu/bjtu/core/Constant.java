/**
 * 2017年6月25日
 */
package cn.edu.bjtu.core;

/**
 * 常量 
 * @author Alex
 *
 */
public class Constant {
	/**
	 * 一般用映射型容器的key
	 * @author Alex
	 *
	 */
	public static class Keys{
		public static final String LabelManagerSaveKey = "text.save"; 
	}
	/**
	 * 值
	 * @author Alex
	 *
	 */
	public static class Values{
		public static final String LabelsFileName = "labels_arr.bin";
		public static final String ModelFileName = "fdcnn_lmy.bin";
		public static final String ConfigFileName = "cnnconfig.properties";
		public static final String MQ_TOPIC = "TEXT-CATE-MQ-TOPIC-TEST";
		
	}
}
