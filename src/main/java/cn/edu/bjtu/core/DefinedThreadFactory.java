package cn.edu.bjtu.core;

import java.io.Serializable;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @ClassName:ThreadLocal
 * @Description: TODO
 * @author: Richard
 * @ChineseName: Yao Li
 * @date: 2018年1月6日 下午10:20:06
 */
public class DefinedThreadFactory implements ThreadFactory,Serializable {
	private static final long serialVersionUID = -6271225030275970882L;
	private static final AtomicInteger poolNumber = new AtomicInteger(1);
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    private final String namePrefix;

    public DefinedThreadFactory() {
        namePrefix = "pool-" +
                      poolNumber.getAndIncrement() +
                     "-thread-";
    }

    public Thread newThread(Runnable r) {
        Thread t = new Thread(null, r,
                              namePrefix + threadNumber.getAndIncrement(),
                              0);
        if (t.isDaemon())
            t.setDaemon(false);
        if (t.getPriority() != Thread.NORM_PRIORITY)
            t.setPriority(Thread.NORM_PRIORITY);
        return t;
    }

}
