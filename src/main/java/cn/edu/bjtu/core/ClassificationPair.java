package cn.edu.bjtu.core;

import java.io.Serializable;

public final class ClassificationPair implements Serializable{
	private static final long serialVersionUID = -5287890610363875793L;
	private final String cate;
	private final double val;
	public ClassificationPair(String cate,double val) {
		this.cate = cate;
		this.val = val;
	}
	
	public String toString(){
		return "{label:"+cate+","+"val:"+val+"}";
	}
	
	public String getLabelName() {
		return cate;
	}
	public double getVal() {
		return val;
	}
	
	
}