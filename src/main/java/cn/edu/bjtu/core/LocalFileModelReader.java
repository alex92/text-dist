/**
 * 
 */
package cn.edu.bjtu.core;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Date;
import java.util.Properties;
import org.apache.commons.io.IOUtils;
import cn.edu.bjtu.api.NetworkModelService;

/**
 * @author alex
 *
 */

public class LocalFileModelReader implements NetworkModelService,Serializable{
	private static final long serialVersionUID = 512890377275355005L;
	Properties p = new Properties();
	byte [] modelByte = null;
	/**
	 */
	public LocalFileModelReader() throws Exception{
		//默认4M
		ByteArrayOutputStream baos = new ByteArrayOutputStream(4*1024);
		// TODO Auto-generated constructor stub
		p.load(this.getClass().getClassLoader().getResourceAsStream("meta/"+Constant.Values.ConfigFileName));
		InputStream input = LocalFileModelReader.class.getClassLoader().getResourceAsStream("meta/"+Constant.Values.ModelFileName);
		IOUtils.copy(input, baos);
		modelByte = baos.toByteArray();
	}
	/* (non-Javadoc)
	 * @see org.textplantform.common.provider.NetworkModelService#lastModifiedTime()
	 */
	@Override
	public Date lastModifiedTime() {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see org.textplantform.common.provider.NetworkModelService#getType()
	 */
	@Override
	public Deep4jModelType getType() {
		// TODO Auto-generated method stub
		return Deep4jModelType.Local;
	}

	/* (non-Javadoc)
	 * @see org.textplantform.common.provider.NetworkModelService#getSentenceLength()
	 */
	@Override
	public int getSentenceLength() {
		// TODO Auto-generated method stub
		return 0;
	}

	/* (non-Javadoc)
	 * @see org.textplantform.common.provider.NetworkModelService#getModelFileStream()
	 */
	@Override
	public byte[] getModelFileStream() {
		// TODO Auto-generated method stub
		return modelByte;
	}

}
