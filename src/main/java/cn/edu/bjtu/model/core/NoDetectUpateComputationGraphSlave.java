/**
 * 
 */
package cn.edu.bjtu.model.core;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.concurrent.Future;

import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import cn.edu.bjtu.api.NetworkModelService;
import cn.edu.bjtu.core.Deep4jModelType;

/**
 * @author alex
 *
 */
public class NoDetectUpateComputationGraphSlave extends Slave{
	
	private static final long serialVersionUID = -7047591294501428008L;
	transient ThreadModel threadModel;
	public NoDetectUpateComputationGraphSlave(NetworkModelService nms) {
		super(nms,false);
		threadModel = new ThreadModel(nms);
	}
	
	private void readObject(ObjectInputStream in) throws IOException,ClassNotFoundException{
		in.defaultReadObject();
		threadModel = new ThreadModel(nms); 
	}
	
	@Override
	public INDArray[] output(INDArray... input) throws Exception {
		ComputationGraph temp = threadModel.get();
		return temp == null?DumpSlave.ALLONE.output(input):temp.output(input);
	}

	@Override
	public Future<INDArray[]> outputAsync(INDArray... input) throws Exception {
		throw new RuntimeException(" Not Support Async in Spark Environment ");
	}

	@Override
	public Evaluation evaluate(DataSetIterator iterator) throws Exception {
		ComputationGraph temp = threadModel.get();
		return temp == null?DumpSlave.ALLONE.evaluate(iterator):temp.evaluate(iterator);
	}

	@Override
	public Future<Evaluation> evaluateAsync(DataSetIterator iterator) throws Exception {
		throw new RuntimeException(" Not Support Async in Spark Environment ");
	}

	@Override
	public Deep4jModelType getActualType() {
		return Deep4jModelType.Local;
	}

	@Override
	public void load() throws Exception {
		logger.info(" lazy loading , each thread loads model when needed ... ");
	}

	@Override
	Object get() {
		return threadModel.get();
	}

}
