package cn.edu.bjtu.model.core;

import java.io.Serializable;

import cn.edu.bjtu.api.NetworkModelService;
import cn.edu.bjtu.api.NeuronNetwork;
import cn.edu.bjtu.core.Deep4jModelType;
import cn.edu.bjtu.core.LoggerSupport;

/**
 * @ClassName:AbstractNetWorkWrapper
 * @Description: TODO
 * @author: Yao Li
 * @date: 2018/1/6  PM 11:19:58
 */
public abstract class AbstractNetWorkWrapper  extends LoggerSupport  implements NeuronNetwork,Serializable{
	private static final long serialVersionUID = 7221818985962605703L;
	protected NetworkModelService nms;
	protected Slave slave = null;
	@Override
	public void load() throws Exception {
		switch(nms.getType()){
			case ComputationGraph:
				slave = new ComputationGraphSlave(nms);
				break;
			case MultiLayerNetwork:
				slave  = new MultiLayerNetworkSlave(nms);
				break;
			case Local:
				slave = new NoDetectUpateComputationGraphSlave(nms);
				break;
			default:
				throw new IllegalArgumentException("unknow type");
		}
		slave.load();
	}

	@Override
	public Deep4jModelType getActualType() {
		return slave.getActualType();
	}
	public Object get(){
		return slave.get();
	}
}
