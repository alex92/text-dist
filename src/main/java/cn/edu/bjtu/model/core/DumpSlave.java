/**
 * 2017年11月22日
 */
package cn.edu.bjtu.model.core;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import org.deeplearning4j.eval.Evaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.factory.Nd4j;

import cn.edu.bjtu.api.NeuronNetwork;
import cn.edu.bjtu.core.Deep4jModelType;

/**
 * 当模型加载失败的时候,返回一个明显错误的结果
 * @author Alex
 *
 */
public enum DumpSlave implements NeuronNetwork{
	ALLZERO {
		@Override
		public INDArray[] output(INDArray... input) {
			//返回全是0的结果
			int size = input.length;
			INDArray res [] = new INDArray[size];
			for(int i=0;i<res.length;i++){
				res[i] = Nd4j.zeros(input[i].length());
			}
			return res;
		}
	},ALLONE {
		@Override
		public INDArray[] output(INDArray... input) {
			//返回全是1的结果
			int size = input.length;
			INDArray res [] = new INDArray[size];
			for(int i=0;i<res.length;i++){
				res[i] = Nd4j.ones(input[i].length());
			}
			return res;
		}
	};

	@Override
	public Future<INDArray[]> outputAsync(INDArray... input) throws Exception {
		CompletableFuture<INDArray[]> result = new CompletableFuture<INDArray[]>();
		result.complete(output(input));
		return result;
	}

	@Override
	public Evaluation evaluate(DataSetIterator iterator){
		return new Evaluation();
	}

	@Override
	public Future<Evaluation> evaluateAsync(DataSetIterator iterator){
		CompletableFuture<Evaluation> result = new CompletableFuture<Evaluation>();
		result.complete(new Evaluation());
		return result;
	}

	@Override
	public Deep4jModelType getActualType() {
		return Deep4jModelType.Fake;
	}

	@Override
	public void load() throws Exception {
	}

	@Override
	public abstract INDArray[] output(INDArray... input);

}
