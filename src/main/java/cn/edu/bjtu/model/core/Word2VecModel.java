/**
 * 
 */
package cn.edu.bjtu.model.core;

import java.io.Serializable;

import org.deeplearning4j.models.embeddings.loader.WordVectorSerializer;
import org.deeplearning4j.models.word2vec.Word2Vec;

import cn.edu.bjtu.api.W2VModelReader;

/**
 * @author alex
 *
 */
class Word2VecModel implements Serializable{
	private static final long serialVersionUID = -4769461398206726550L;
	Word2Vec w2v = null;
	public Word2Vec getW2v(){
		return w2v;
	}
	public Word2VecModel(W2VModelReader reader) throws Exception {
		try{
			w2v = WordVectorSerializer.readWord2VecModel(reader.getW2vFile());
		}finally{
			reader.release();
		}
	}
}
