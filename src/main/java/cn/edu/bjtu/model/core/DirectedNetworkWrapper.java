package cn.edu.bjtu.model.core;

import java.io.Serializable;
import java.util.concurrent.Future;

import org.deeplearning4j.eval.Evaluation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import cn.edu.bjtu.api.NetworkModelService;


/**
 * @ClassName:DirectedNetworkWrapper
 * @Description: TODO
 * @author: Yao Li
 * @date: 2018/1/6 PM11:22:38
 */
public class DirectedNetworkWrapper extends AbstractNetWorkWrapper implements Serializable {

	private static final long serialVersionUID = -4702784215804802386L;

	public DirectedNetworkWrapper(NetworkModelService nms) {
		super.nms = nms;
	}
	
	@Override
	public INDArray[] output(INDArray... input) throws Exception {
		return slave.output(input);
	}

	@Override
	public Future<INDArray[]> outputAsync(INDArray... input) throws Exception {
		return slave.outputAsync(input);
	}

	@Override
	public Evaluation evaluate(DataSetIterator iterator) throws Exception {
		return slave.evaluate(iterator);
	}

	@Override
	public Future<Evaluation> evaluateAsync(DataSetIterator iterator) throws Exception {
		return slave.evaluateAsync(iterator);
	}

}
