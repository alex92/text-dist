/**
 * 2017年11月22日
 */
package cn.edu.bjtu.util;

import org.deeplearning4j.iterator.LabeledSentenceProvider;
import org.deeplearning4j.models.word2vec.Word2Vec;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import cn.edu.bjtu.datasource.dsiter.LengthExceptionDetectCNNDataSetIterator;

/**
 * @author Alex
 *
 */
public class DatasetUtils {
	private DatasetUtils(){}
	
	public static DataSetIterator getCNNDataSet(TokenizerFactory tf,
			LabeledSentenceProvider sp,int batch,int senLen,Word2Vec w2v){
			return new LengthExceptionDetectCNNDataSetIterator.Builder()
				.tokenizerFactory(tf)
		        .sentenceProvider(sp)
		        .wordVectors(w2v)
		        .minibatchSize(batch)
		        .maxSentenceLength(senLen)
		        .useNormalizedWordVectors(false)
		        .build();
	}
	public static DataSetIterator getCNNDataSet(LabeledSentenceProvider sp,int batch,int senLen,Word2Vec w2v){
			return new LengthExceptionDetectCNNDataSetIterator.Builder()
				.tokenizerFactory(TokenizationUtils.getTokenizerFactory())
		        .sentenceProvider(sp)
		        .wordVectors(w2v)
		        .minibatchSize(batch)
		        .maxSentenceLength(senLen)
		        .useNormalizedWordVectors(false)
		        .build();
	}
	
}
