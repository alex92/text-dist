/**
 * 2017年11月22日
 */
package cn.edu.bjtu.util;

import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;

import cn.edu.bjtu.tokenization.AnsjTokenzierFactory;


/**
 * @author Alex
 *
 */
public class TokenizationUtils {
	private TokenizationUtils(){}
	public static TokenizerFactory getTokenizerFactory(){
		return new AnsjTokenzierFactory(false);
	}
}
