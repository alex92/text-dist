/**
 * 2017年6月21日
 */
package cn.edu.bjtu.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.datavec.api.records.reader.impl.LineRecordReader;
import org.datavec.api.split.FileSplit;
import org.datavec.api.util.files.ShuffledListIterator;
import org.datavec.api.writable.Text;

import cn.edu.bjtu.core.LoggerSupport;


/**
 * @author Alex
 *
 */
public class RandomShuffle extends LoggerSupport{
	File in;
	File out;
	double ratio ;
	String suffix;
	int seed;
	private Random rand = new Random(seed);
	private List<String> list ;
	protected RandomShuffle(Builder builder){
		this.in = builder.inputFile;
		this.out = builder.outputDir == null?in.getParentFile():builder.outputDir;
		this.ratio = builder.ratio;
		this.seed = builder.seed;
		this.suffix = builder.suffix;
		list = new ArrayList<>(10000);
	}
	//最小的数是Min,最大是数是Max返回其随机顺序
	private int [] order(int min,int max){
		int len = max-min+1;
		int []src = new int[len];
		for(int i=0;i<src.length;i++){
			src[i] = min+i;
		}
		int res[] = new int[src.length];
		for(int i=0;i<res.length;i++){
			int index = Math.abs( rand.nextInt() % len--);
			res[i] = src[index];
			src[index] = src[len];
			
		}
		return res;
	}
	public void fit() throws Exception{
		try(LineRecordReader lrr = new LineRecordReader()){
			lrr.initialize(new FileSplit(in));
			while(lrr.hasNext()){
				Text t = (Text) lrr.next().get(0);
				list.add(t.toString());
			}
		}
		ShuffledListIterator<String> rli = new ShuffledListIterator<>(list, order(0,list.size()-1));
		File target = out == null?new File(in.getParent()):out;
		try(BufferedWriter br = new BufferedWriter(new FileWriter(new File(target,in.getName()+suffix)))){
			while(rli.hasNext()){
				br.write(rli.next());
				br.newLine();
			}
		}
	}
	public static class Builder{
		File inputFile;
		File outputDir;
		double ratio;
		String suffix = "_out";
		int seed = 47;
		public Builder() {
		}
		public Builder setInputFile(File inputFile) {
			this.inputFile = inputFile;
			return this;
		}
		public Builder setOutputDir(File outputDir) {
			this.outputDir = outputDir;
			return this;
		}
		public Builder setRatio(double ratio) {
			this.ratio = ratio;
			return this;
		}
		public Builder setSuffix(String suffix) {
			this.suffix = suffix;
			return this;
		}
		public Builder setSeed(int seed) {
			this.seed = seed;
			return this;
		}
		public RandomShuffle build(){
			return new RandomShuffle(this);
		}
		
	}
	public static void main(String args[]) throws Exception{
		
		RandomShuffle rs = new RandomShuffle.Builder().setInputFile(new File("D:\\textdata\\userData\\c3\\1.txt")).build();
		rs.fit();
		
//		Random rand = new Random();
//		int min = 0;
//		int max = 5;
//
//		int len = max-min+1;
//		int []src = new int[len];
//		for(int i=0;i<src.length;i++){
//			src[i] = min+i;
//		}
//		int res[] = new int[src.length];
//		for(int i=0;i<res.length;i++){
//			int index = Math.abs( rand.nextInt() % len--);
//			res[i] = src[index];
//			src[index] = src[len];
//			
//		}
//		System.out.println(Arrays.toString(res));
	}
	
}
