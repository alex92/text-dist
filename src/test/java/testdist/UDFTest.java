package testdist;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import cn.edu.bjtu.model.core.TextClassificationModel;


/**
 * @ClassName:UDFTest
 * @Description: TODO
 */
public class UDFTest {
	@SuppressWarnings("deprecation")
	public static void main(String[] args) {
		SparkConf conf = new SparkConf().setAppName("UDF").setMaster("local");
		JavaSparkContext sc = new JavaSparkContext(conf);
		SQLContext sqlContext = new SQLContext(sc);
		
		
		JavaRDD<String> lines = sc.textFile("C:/Users/lenovo/Desktop/testdata.txt",3);
		
		//转换为JavaRDD<Row>
		JavaRDD<Row> wechatRDD = lines.map(new Function<String, Row>() {
			@Override
			public Row call(String line) throws Exception {
				String[] splited = line.split("\t");
				return RowFactory.create(splited[0], splited[8]);
			}
		});

		
		
		List<StructField> structFields = new ArrayList<StructField>();
		structFields.add(DataTypes.createStructField("id", DataTypes.StringType, true));
		structFields.add(DataTypes.createStructField("content", DataTypes.StringType, true));
		StructType structType = DataTypes.createStructType(structFields);
		Dataset<Row> wechatDF = sqlContext.createDataFrame(wechatRDD, structType);
		//注册数据库表
		wechatDF.registerTempTable("wechatcontent");
		
		//注册UDF类
		sqlContext.udf().register("classification", new TestClassification(),DataTypes.StringType);
		List<Row> rows = sqlContext.sql("SELECT id, classification(content) FROM wechatcontent").javaRDD().collect();
		for (Row row : rows) {
			System.out.println("id:" + row.get(0) + "\t\t\t\t\t and result：" + row.get(1));
		}
		sc.close();

	}
}