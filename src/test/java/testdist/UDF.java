package testdist;

import java.util.Arrays;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.api.java.UDF1;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.clearspring.analytics.util.Lists;

import testdist.model.ClassifyUDF;
import testdist.model.TestUDF1;

/**
 * @ClassName:UDF
 * @Description: TODO
 * @author: Richard
 * @ChineseName: Yao Li
 * @date: 2018年3月9日 下午4:11:45
 */
public class UDF {
	static Logger logger = LoggerFactory.getLogger(TestUDF1.class);
    public static void main(String[] args) {
    	
        SparkConf conf = new SparkConf().setAppName("UDF").setMaster("local");
        JavaSparkContext sc = new JavaSparkContext(conf);
        SQLContext sqlContext = new SQLContext(sc);
        List<String> nameList = Arrays.asList("中国是一个历史悠久的国家", "太阳中国历史哲学", 
        		"它是记载和解释一系列人类活动进程的历史事件的一门学科，","延伸的。历史是文化的传承，积累",
        		"如何在现实中可能成为可以讨论","文化的传承，积累和扩展，是人类文明的",
        		"历史随时产生，是人们在过去自由活动的如实记录。",
        		"对人类社会过去的事件和行动，以及对这些事件行","中国是一个历史悠久的国家", "太阳中国历史哲学", 
        		"它是记载和解释一系列人类活动进程的历史事件的一门学科，","延伸的。历史是文化的传承，积累",
        		"如何在现实中可能成为可以讨论","文化的传承，积累和扩展，是人类文明的",
        		"历史随时产生，是人们在过去自由活动的如实记录。",
        		"对人类社会过去的事件和行动，以及对这些事件行");
        //转换为javaRDD
        JavaRDD<String> nameRDD = sc.parallelize(nameList, 5);
        //转换为JavaRDD<Row>
        JavaRDD<Row> nameRowRDD = nameRDD.map(new Function<String, Row>() {
            public Row call(String name) throws Exception {
                return RowFactory.create(name);
            }
        });
        List<StructField> fields = Lists.newArrayList();
        fields.add(DataTypes.createStructField("name", DataTypes.StringType,true));
        StructType structType = DataTypes.createStructType(fields);
        Dataset<Row> namesDF = sqlContext.createDataFrame(nameRowRDD, structType);
        //注册names表
        namesDF.registerTempTable("names");
        /**
         * Function可以使用UDF1到UDF22/21?,所表达的意思就是几个参数，2代指两个入参，10代指10个入参
         * return返回的即为UDF<>的最后一个参数，
         */
        UDF1 udf1 = null;
        try{
        	udf1 = new ClassifyUDF();
        }catch(Exception e){
        	logger.info("exception : {}",e);
        }
        //sqlContext.udf().register("strLen", new TestUDF1(),DataTypes.IntegerType);
        sqlContext.udf().register("strLen", udf1,DataTypes.StringType);
        
        List<Row> rows = sqlContext.sql("select name,strLen(name) from names").javaRDD().collect();
        for (Row row : rows) {
        	logger.info("\nname:{}, 长度：{}",row.get(0),row.get(1));
        }
        sc.close();
    }
}
