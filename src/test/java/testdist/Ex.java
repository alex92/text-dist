/**
 * 2018年1月26日
 */
package testdist;

import javax.management.RuntimeErrorException;

/**
 * @author Alex
 *
 */
public class Ex {
	public static void main(String[] args) throws Exception {
		method1();
	}
	static void method1() throws Exception {
		try{
			method2();
		}catch(RuntimeException e){
			//e.printStackTrace();
			throw new Exception(e);
		}
	}
	static void method2(){
		throw new RuntimeException("test ex");
	}
}
