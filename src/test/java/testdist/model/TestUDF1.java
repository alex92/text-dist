/**
 * 2018年3月12日
 */
package testdist.model;

import org.apache.spark.sql.api.java.UDF1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alex
 *
 */
public class TestUDF1 implements UDF1<String, Integer> {
	 Logger logger = LoggerFactory.getLogger(TestUDF1.class);
	 private static final long serialVersionUID = -644588536063108039L;
	 /**
	 * 
	 */
	public TestUDF1() {
		logger.info(" Test UDF1 ctor .... ");
	}
	public Integer call(String s) throws Exception {
         return s.length();
     }
	
	
}
