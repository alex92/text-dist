/**
 * 2018年3月12日
 */
package testdist.model;

import java.util.Arrays;

import org.apache.spark.sql.api.java.UDF1;

import cn.edu.bjtu.model.core.TextClassificationModel;

/**
 * @author Alex
 *
 */
public class ClassifyUDF implements UDF1<String, String>  {
	private static final long serialVersionUID = -4650694551342889306L;
	TextClassificationModel model;
	public ClassifyUDF() throws Exception {
		model = new TextClassificationModel();
	}
	
	@Override
	public String call(String t1) throws Exception {
		return Arrays.toString(model.classifyDocument(t1));
	}

}
