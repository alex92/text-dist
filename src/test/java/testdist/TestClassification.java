package testdist;

import org.apache.spark.sql.api.java.UDF1;

import cn.edu.bjtu.core.ClassificationPair;
import cn.edu.bjtu.model.core.TextClassificationModel;

/**
 * @ClassName:TestClassificationModel
 * @Description: TODO
 */
public class TestClassification implements UDF1<String, String> {
	private static final long serialVersionUID = 1L;

	private TextClassificationModel model;

	public TestClassification() {
		super();
		//之前在此处初始化分类模型
		/*try {
			model = new TextClassificationModel();
		} catch (Exception e) {
			e.printStackTrace();
		}*/

	}

	@Override
	public String call(String article) throws Exception {
		StringBuffer sb = new StringBuffer();
		try {
			//改成在此处初始化分类模型 ok
			model = new TextClassificationModel();
			ClassificationPair[] classifiResults = model.classifyDocument(article);
			for (ClassificationPair classifiresult : classifiResults) {
				sb.append(classifiresult.getLabelName() + "\t" + classifiresult.getVal() + ";");
			}
			sb.deleteCharAt(sb.length() - 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

}
